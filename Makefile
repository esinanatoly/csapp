all: server client1 client2

proto: messages.proto
	@protoc-c --c_out=. messages.proto 

server: server.c server.h servermain.c messages.pb-c.c
	gcc -std=gnu99 server.c server.h servermain.c messages.pb-c.c -o server -pthread -lprotobuf-c
client1: client1.c client.h client.c messages.pb-c.c
	gcc -std=gnu99 client1.c client.h client.c messages.pb-c.c  messages.pb-c.h -o client1 -lprotobuf-c
client2: client2.c client.h client.c messages.pb-c.c
	gcc -std=gnu99 client2.c client.h client.c messages.pb-c.c messages.pb-c.h -o client2 -lprotobuf-c
test: server client1 client2
	x-terminal-emulator -e ./server 127.0.0.1
	x-terminal-emulator -e ./client1
	x-terminal-emulator -e ./client2
cleanproto:
	@rm -rf messages.pb-c.c messages.pb-c.h
clean: 
	rm -rf server client1 client2
