#include "server.h"
#include "messages.pb-c.h"
int get_sem_val(int semid)
{
    return(semctl(semid,0,GETVAL,0));
}

int sem_create() // инициализирцем семафор
  {
    union semun 
     {
      int val;
      struct semid_ds *buf;
      unsigned short *array;
      struct seminfo *__buf; 
     };

     int semid;
     semid = semget(IPC_PRIVATE, 1, 0666 | IPC_CREAT);
     printf("Создан семофор : %d\n",semid);
     union semun arg;
     arg.val = QUEUE;
     semctl(semid, 0, SETVAL, arg);
     return semid;
  }
  
int msg_create() // инициализация очереди сообщений
  {
   int mqid;
   int msgflg = 0600 | IPC_CREAT;
   if( (mqid = msgget(IPC_PRIVATE,msgflg)) >= 0 )
      printf("Создана очередь сообщений id = %d\n",mqid);
   else
      return -1;
  return mqid;
  } 



int CreateTCPServerSocket(unsigned short port)
  {
    int sock; 
    struct sockaddr_in ServAddr; 
    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
      {	
    	perror("Сервер: ошибка при создании TCP socket()");
      sleep(5);
    	exit(1);	
      }
    memset(&ServAddr, 0, sizeof(ServAddr));
    ServAddr.sin_family = AF_INET;
    ServAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    ServAddr.sin_port = htons(port);
      int yes=1;
  //избавляемся от ошибки "Address already in use"
    if (setsockopt(sock,SOL_SOCKET,SO_REUSEADDR|SO_REUSEPORT,&yes,sizeof(int)) == -1)
     {
      perror("setsockopt");
      exit(1);
      }
    if (bind(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0)
      {	
    	perror("Сервер: ошибка при связывании порта и адреса bind()");
      sleep(5);
    	exit(1);
      }
    if (listen(sock, QUEUE) < 0)
      {	
    	perror("Сервер: ошибка при listen()");
      sleep(5);
    	exit(1);	
      }
    return sock;
  }  

int AcceptTCP(int servSock)
  {
    int clntSock; 
    struct sockaddr_in ClntAddr; 
    unsigned int clntLen; 
    clntLen = sizeof(ClntAddr);
    if ((clntSock = accept(servSock, (struct sockaddr *) &ClntAddr, &clntLen)) < 0)
      {	
    	perror("Сервер: ошика при accept()");
      sleep(5);
    	exit(1);	
      }
    printf("Сервер: создан сокет для клиента  %s\n", inet_ntoa(ClntAddr.sin_addr));
    return clntSock;
  }

 void * ClientThread(void * arg)
  {
	pthread_detach(pthread_self());
	int connSock=((client_str *)arg)->connSock;
	int msqid=((client_str *)arg)->msqid;
	int semid=((client_str *)arg)->semid;
	int type=((client_str *)arg)->type;
	free(arg);
  uint8_t buf[MAX_READ_PROTOBUF];
  int bytes_recv=0;
	Serv_msg message;
  TCPMSG *msg_tcp;
	switch(type)
        {
         case 1:
             printf("Сервер: Client1 thread %lu\n", pthread_self());
             
	           bytes_recv=recv(connSock,&buf[0],MAX_READ_PROTOBUF,0);
	           printf("Сервер принял TCP сообщение\n");
	           close(connSock);
             //запись в очередь
             struct sembuf lock={0,-1,0}; //для блокировки ресурса
             if (semop(semid,&lock,1)==-1) //уменьшаем семафор
                { 
                  perror("Сервер: semlock failed"); 
                }
             msg_tcp=tcp__msg__unpack(NULL,bytes_recv,buf);
             printf("Добавлено в очередь (время,длина,строка)| %d | %d | %s\n",msg_tcp->time,msg_tcp->len_string,msg_tcp->data);  
             tcp__msg__free_unpacked(msg_tcp,NULL);
             message.mtype=MSGTYPE;
            // message.datastorage = malloc(bytes_recv);
             memcpy(message.datastorage,buf,bytes_recv);
             if (msgsnd(msqid,&message,bytes_recv,0)<0)
                { 
                 perror("Сервер: msgsnd() faled");
                 sleep(5);
                 exit(1);
                }
             //free(message.datastorage);   
         break;
         case 2:
             printf("Сервер: Client2 thread %lu\n", pthread_self());
             bytes_recv=msgrcv(msqid,&message,MAX_READ_PROTOBUF,100,0);
             memcpy(buf,message.datastorage,bytes_recv);
             msg_tcp=tcp__msg__unpack(NULL,bytes_recv,buf);
             sleep(3);
             printf("Изъято из очереди (время,длина,строка)| %d | %d | %s\n",msg_tcp->time,msg_tcp->len_string,msg_tcp->data);
	           if ((send(connSock,buf,bytes_recv,0))!=bytes_recv)
               {
             	 perror("Сервер: send() failed");
               sleep(5);
		           exit (1);
		           }
	           printf("Сервер отправил TCP сообщение\n");
             tcp__msg__free_unpacked(msg_tcp,NULL);
	           struct sembuf unlock={0,1,0}; //для разблокировки ресурса
	           semop(semid,&unlock,1); //увеличиваем семафор
	           close(connSock);
         break;  
         default:
         break; 
        } 
    pthread_exit(NULL);
  }

 
void * ThreadTCP(void * arg)
  {
    printf("Сервер: TCP поток %lu\n", pthread_self());
    int type=((TCP_struct *)arg)->type;
    int msqid=((TCP_struct *)arg)->msqid;
    int semid=((TCP_struct *)arg)->semid;
    free(arg);
    unsigned short tcp_port;
    switch(type)
          {
             case 1:
                tcp_port=atoi(TCP1_PORT);
             break;
             case 2:
                tcp_port=atoi(TCP2_PORT);  
             break;  
             default:
             break; 
          } 
    int listenSock,connSock;
	listenSock=CreateTCPServerSocket(tcp_port);
	while(1)
	 {	
	 	connSock=AcceptTCP(listenSock);
        pthread_t client_tid;
        client_str *client_arg;
        client_arg=(client_str *)malloc(sizeof(client_str));
        if (client_arg == NULL)
           {	
        	perror("Сервер: ошибка выделения памяти  malloc");
          sleep(5);
			    exit(1);	
		   }
		client_arg->connSock=connSock;
		client_arg->msqid=msqid;
		client_arg->semid=semid;
		client_arg->type=type;
        if (pthread_create(&client_tid, NULL, ClientThread, (void *)client_arg)!=0)
          {
           perror("Сервер: ошибка при создании потока pthread_create");
           sleep(5);
           exit(1);	
          }  
	 }
   pthread_exit(NULL);
  }
 
void * ThreadUDP(void * arg)
  {
  UDPMSG msg = UDP__MSG__INIT; // AMessage
  void *buf;                     // Buffer to store serialized data
  unsigned len;                  // Length of serialized data
  int UDP_PORT=((UDP_struct *)arg)->udp_p;
	char *broadcast_msg=((UDP_struct *)arg)->tcp_p;
	int semid=((UDP_struct *)arg)->semid;
	int type=((UDP_struct *)arg)->type;
	free(arg);
	printf("Сервер: UDP%d port %d\n", type, UDP_PORT);
	int udp_sock;
	int broadcastPermission;
	struct sockaddr_in broadcastaddr;
	memset(&broadcastaddr, 0, sizeof(broadcastaddr));
	broadcastaddr.sin_family = AF_INET;
	broadcastaddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	broadcastaddr.sin_port = htons(UDP_PORT);
	if ((udp_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))<0)
	   {	
		perror("Сервер: ошибка при создании UDP сокета socket()");
		sleep(5);
    exit(1);
	   }
	broadcastPermission=1;
	if (setsockopt(udp_sock, SOL_SOCKET, SO_BROADCAST,(void *)&broadcastPermission, sizeof(broadcastPermission)) == -1)
       { 
	    perror("Сервер: ошибка в setsockopt()");
		  sleep(5);
      exit(1);	
	   }
	while(1)
	{
     switch(type)
          {
           case 1:
                if(get_sem_val(semid)) //в очереди есть места
                  {
                    msg.data=broadcast_msg;
                    len = udp__msg__get_packed_size(&msg);
                    buf = malloc(len);
                    udp__msg__pack(&msg,buf);
                   if((sendto(udp_sock, buf, len, 0,
				   (struct sockaddr *)&broadcastaddr, sizeof(broadcastaddr)))!=len)
                    {	
                     perror("Сервер: ошибка  sendto()");
                     sleep(5);
                     exit(1);	
                    }
                   printf("Сервер: UDP%d послал широковещательное сообщение\n", type);
                   free(buf);
                   sleep(UDP_SLEEP); 
                  }
           break;
           case 2:
                 if(get_sem_val(semid)<QUEUE) //в очереди есть сообщения
                  {
                    msg.data=broadcast_msg;
                    len = udp__msg__get_packed_size(&msg);
                    buf = malloc(len);
                    udp__msg__pack(&msg,buf);
                   if((sendto(udp_sock, buf, len, 0,
				   (struct sockaddr *)&broadcastaddr, sizeof(broadcastaddr)))!=len)
					{	
					  perror("Сервер: ошибка  sendto()");
                      sleep(5);
                      exit(1);	
                     }
                   printf("Сервер: UDP%d послал широковещательное сообщение\n", type);
                   free(buf); 
                   sleep(UDP_SLEEP);
                  } 
           break;  
           default:
           break; 
          } 
	}
	//недостижимый участок кода
	close(udp_sock);
	pthread_exit(NULL);

  }

void deinit(int mqid,int semid) // удаляем очередь и семафор
 {
 	int c;
 	while(1)
    {   c = getc(stdin);
		switch(c)
		{   case 'q':
           if(semctl(semid, 0, IPC_RMID) >= 0)
              printf("Удален семафор %d\n",semid);
           if(msgctl(mqid, IPC_RMID, NULL)>=0)
              printf("Удалена очередь %d\n",mqid);
           raise(SIGTERM);
          break;
          default:
          break; 
        }
    }    
 }