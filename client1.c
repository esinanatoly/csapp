#define UDP_PORT1 4900
#include "client.h"
#include "messages.pb-c.h"


int main (void)
{
    srand(time(NULL));
	int udp_sock, recvStringLen;
	char recvString[MAXRECVSTRING];
	struct sockaddr_storage their_addr;
    socklen_t addr_len;
    addr_len=sizeof(their_addr);
  	while(1)
    {
        //создаем UDP-сокет, привязываемся к порту
        udp_sock=UDP_Init(UDP_PORT1);
       	printf("Client %d: waiting UDP notification...\n",getpid());
        // ожидаем UDP-cooбщение
        uint8_t buf[MAX_UDPMSG_SIZE];
        if ((recvStringLen = recvfrom(udp_sock, &buf[0], MAX_UDPMSG_SIZE,0,
			(struct sockaddr *)&their_addr, &addr_len)) < 0)
        {	perror("Client: error in recvfrom()");	}
        else //если приняли
        { 
            UDPMSG *msg;
            msg = udp__msg__unpack(NULL, recvStringLen, buf); 
            short unsigned ServTCPport=atoi(msg->data); // номер порта, принятый от сервера через сообщение
            char ipstr[INET_ADDRSTRLEN]; // IP-адрес сервера
            inet_ntop(their_addr.ss_family,get_in_addr((struct sockaddr *)&their_addr), ipstr, sizeof(ipstr));
            printf("Client %d received TCP port: %d, %d bytes, IP %s \n",getpid(), ServTCPport, recvStringLen, ipstr);
            close(udp_sock); //закрываем UDP-соединение
            udp__msg__free_unpacked(msg, NULL);
            //подключаемся к TCP
            int tcp_sock = TCP_Init(ServTCPport, ipstr);

            //формируем заявку серверу
            TCPMSG msg_tcp = TCP__MSG__INIT;
            void *buf;
            unsigned int len;
            msg_tcp.len_string = (int32_t)(rand()%11+10);
            msg_tcp.time = (int32_t)(rand()%5+1);
            char stringrand[25];
            int i = 0;
            for (i = 0;i<msg_tcp.len_string;i++) stringrand[i] = (char) (rand()%25+97); stringrand[msg_tcp.len_string] = '\0';
            msg_tcp.data=stringrand;
            len = tcp__msg__get_packed_size(&msg_tcp);
            buf = malloc(len);
            tcp__msg__pack(&msg_tcp,buf);
            if ((send(tcp_sock, buf, len,0))!=len)
            {	perror("Client: send() failed");	}
            printf("Отправляю на сервер сообщение(сон = %d) : %s\n",msg_tcp.time,msg_tcp.data);
            printf("Client %d sleep\n\n", getpid());
            sleep(msg_tcp.time);
            free(buf);
            close(tcp_sock);
        }

    }
	return EXIT_SUCCESS;
}