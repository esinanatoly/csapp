#include <sys/types.h> //socket data types
#include <sys/ipc.h>
#include <sys/msg.h> //queue
#include <sys/sem.h> //semaphores
#include <sys/socket.h> //socket(), connect(), send(), recv()
#include <stdio.h> //printf()
#include <stdlib.h>
#include <unistd.h> // close(), sleep()
#include <string.h> //memset()
#include <errno.h> //errno()
#include <pthread.h>
#include <arpa/inet.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

//МАКРОСЫ
#define UDP1_PORT 4900 //до 4900+CLIENT1
#define UDP2_PORT 5900 //до 5900+CLIENT2
#define TCP1_PORT "6900"
#define TCP2_PORT "7900"
#define CLIENT_TYPE1 3 //определяет число UDP портов для клиентов первого типа
#define CLIENT_TYPE2 3 //определяет число UDP портов для клиентов второго типа
#define MAX_READ_PROTOBUF 127 // максимальный объем данных от клиентов по TCP
#define QUEUE 2 //размер очереди
#define MAXSTRING 255 //max число байт, принимаемое и передаваемое по TCP
#define MSGTYPE 100
#define UDP_SLEEP 2

struct client_struct
{
    int connSock; //сокет соединения
    int msqid;//дескриптор очереди
    int semid; //дескриптор семафора
    int type;
};
typedef struct client_struct client_str;

struct UDP_struct
{
    int type; //1 - есть место в очереди, 2 - есть заявки в очереди
    int udp_p; //порт UDP, с которого ведется бродкаст
    char *tcp_p; //порт TCP (клиентам 1го типа -> TCP1_PORT, клиентам 2го типа -> TCP2_PORT)
    int semid; //дескриптор семафора
    char ipserv[16];
};
typedef struct UDP_struct UDP_struct;

struct TCP_struct
{
    int type; //1-receive request, 2- send request
    int msqid; //дескриптор очереди
    int semid; //дескриптор семафора
};
typedef struct TCP_struct TCP_struct;

struct Serv_msg
{
    long mtype;
    uint8_t datastorage[127];
};
typedef struct Serv_msg Serv_msg;

int get_sem_val(int semid);
int sem_create();
int msg_create();
int CreateTCPServerSocket(unsigned short port);
int AcceptTCP(int servSock);
void * ClientThread(void * arg);
void * ThreadTCP(void * arg);
void * ThreadUDP(void * arg);
void deinit(int mqid,int semid);
