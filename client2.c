#define UDP_PORT2 5900
#include "client.h"
#include "messages.pb-c.h"

int main (void)
{
	int udp_sock;
	struct sockaddr_storage their_addr;
    socklen_t addr_len;
    addr_len=sizeof(their_addr);
	while(1)
    {
        //создаем UDP-сокет, привязываемся к порту
        udp_sock=UDP_Init(UDP_PORT2);
        printf("Client %d: waiting UDP notification...\n",getpid());
        // ожидаем UDP-cooбщение
        uint8_t buf_udp[MAX_UDPMSG_SIZE];
        int bytes_recv=0;
        if ((bytes_recv = recvfrom(udp_sock, &buf_udp[0], MAX_UDPMSG_SIZE,0,
            (struct sockaddr *)&their_addr, &addr_len)) < 0)
        {	perror("Client: error in recvfrom()");	}
        else //если приняли
        {
            UDPMSG *msg_udp;
            msg_udp = udp__msg__unpack(NULL, bytes_recv, buf_udp); 
            short unsigned ServTCPport=atoi(msg_udp->data); // номер порта, принятый от сервера через сообщение
            char ipstr[INET_ADDRSTRLEN]; // IP-адрес сервера
            inet_ntop(their_addr.ss_family,get_in_addr((struct sockaddr *)&their_addr), ipstr, sizeof(ipstr));
            printf("Client %d received TCP port: %d, %d bytes, IP %s \n",getpid(), ServTCPport, bytes_recv, ipstr);
            close(udp_sock); //закрываем UDP-соединение
            udp__msg__free_unpacked(msg_udp, NULL);
            //подключаемся к TCP
            int tcp_sock = TCP_Init(ServTCPport, ipstr);
            //принимаем заявку от сервера
            uint8_t buf_tcp[MAX_TCPMSG_SIZE];
            if ((bytes_recv=recv(tcp_sock,buf_tcp, MAX_TCPMSG_SIZE,0))==-1)
            {	perror("Client: recv() failed");
                return EXIT_FAILURE;    }
            TCPMSG *msg_tcp;
            msg_tcp=tcp__msg__unpack(NULL,bytes_recv,buf_tcp);
            printf("Получил сообщение(длина %d, сон %d) : %s\n",msg_tcp->len_string,msg_tcp->time,msg_tcp->data);
            printf("Client %d sleep\n\n", getpid());
            sleep(msg_tcp->time);
            tcp__msg__free_unpacked(msg_tcp, NULL);
            close(tcp_sock);
        }
    }
	return EXIT_SUCCESS;
}